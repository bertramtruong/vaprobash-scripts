#!/usr/bin/env bash

# Update APT sources to use AU repository
sudo sed -i 's/us.archive.ubuntu.com/unused-string-for-replacement/' /etc/apt/sources.list
sudo sed -i 's/archive.ubuntu.com/unused-string-for-replacement/' /etc/apt/sources.list
sudo sed -i 's/unused-string-for-replacement/au.archive.ubuntu.com/' /etc/apt/sources.list
sudo sed -i 's/security.ubuntu.com/au.archive.ubuntu.com/' /etc/apt/sources.list
