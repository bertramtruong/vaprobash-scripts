#!/bin/sh

# Update all APT sources
sudo apt-get update

# Upgrade all installed packages
sudo apt-get upgrade -y